Please use [opsworks-cookbooks-template](https://bitbucket.org/sasaki_dev/opsworks-cookbook-template) instead.

opsworks-cookbooks
==================

**This branch is deprecated and no longer used.**

Merged remote repositories:

  * https://github.com/gouldingken/opsworks-cookbooks
  * https://github.com/aws/opsworks-cookbooks (track for updates)

Find the currently deployed cookbooks in one of these branches, depending on your stack's
[configuration manager](http://docs.aws.amazon.com/opsworks/latest/APIReference/API_CreateStack.html)
  * [chef-11.10](https://bitbucket.org/sasaki_dev/opsworks-cookbooks/branch/chef-11.10) (based on Ken's 11.10 and AWS' release-chef-11.10 branches)


See also <https://aws.amazon.com/opsworks/>

LICENSE: Unless otherwise stated, cookbooks/recipes originated by
Amazon Web Services are licensed under the
[Apache 2.0 license](http://aws.amazon.com/apache2.0/). See the
LICENSE file. Some files are just imported and authored by
others. Their license will of course apply.